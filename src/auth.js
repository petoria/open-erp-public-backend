const jwt = require("jsonwebtoken");
const fs = require("fs");
const cert = fs.readFileSync('./fingerprint/rsa-public.key');

module.exports.withAuth = (cb) => {
    return async (req, res) => {
        if(req.headers["authentication"]){
            let token = req.headers["authentication"];
            let verification = jwt.verify(token, cert);

            return await cb(req, res, verification)
        }
        res.code(403);
        return {status: "forbidden"};
    }
};