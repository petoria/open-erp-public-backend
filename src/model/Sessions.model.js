const Sequelize = require("sequelize");

module.exports.register = function () {
    return {
        session_id: {
            type: Sequelize.STRING,
            allowNull: false,
            primaryKey: true,
            unique: "session_unique"
        },
        dns: {
            type: Sequelize.STRING,
            allowNull: false
        },
        port: {
            type: Sequelize.BIGINT(5)
        },
        fingerprint: {
            type: Sequelize.TEXT
        },
        description: {
            type: Sequelize.STRING
        },
        name: {
            type: Sequelize.STRING
        },
        location: {
            type: Sequelize.STRING
        },
    };
};

module.exports.registered = function(table){

};

module.exports.default_data = function () {
    return [
        {
            session_id: "asdrubale",
            dns: "https://open-erp-plan-backend.herokuapp.com",
            port: 443,
            fingerprint: "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvf7ZCyc718wZzSqZlfii\ncjtCNRs/MdNrR6kysGjlGqXhRCvX49DCVeCbxEtWSLMCEcwFvDs3SwxDJm/y5wzc\nhIljy9+DU4VH29DQaguDENlqR8AQMrikJ5ztdUlvmN0FK20jNQAxiiUeR0WG0n4i\nWw2s5kzawGRvxT9Dr5RHwI/Fwn4AX+JG0ycaDtjGAPF49MJQBOPeP4SHbsxnzIwm\n6yiKqyPZl5vCXPDgruVrhNcDBEDjavuTlzzVOnQc28ixp0f/t0k9iUzRMjq65wro\nsTn0oYn8AKOHOAeZpMSK9CnMYRUwNbcU8Qpz9GJH57Bzm3Ytw2B75c+EgETXzVto\nZQIDAQAB\n-----END PUBLIC KEY-----",
            description: "una descrizione lunga",
            name: "VERONACORP",
            location: "lodi"
        },
        {
            session_id: "qwerty",
            dns: "https://open-erp-plan-backend.herokuapp.com",
            port: 443,
            fingerprint: "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvf7ZCyc718wZzSqZlfii\ncjtCNRs/MdNrR6kysGjlGqXhRCvX49DCVeCbxEtWSLMCEcwFvDs3SwxDJm/y5wzc\nhIljy9+DU4VH29DQaguDENlqR8AQMrikJ5ztdUlvmN0FK20jNQAxiiUeR0WG0n4i\nWw2s5kzawGRvxT9Dr5RHwI/Fwn4AX+JG0ycaDtjGAPF49MJQBOPeP4SHbsxnzIwm\n6yiKqyPZl5vCXPDgruVrhNcDBEDjavuTlzzVOnQc28ixp0f/t0k9iUzRMjq65wro\nsTn0oYn8AKOHOAeZpMSK9CnMYRUwNbcU8Qpz9GJH57Bzm3Ytw2B75c+EgETXzVto\nZQIDAQAB\n-----END PUBLIC KEY-----",
            description: "Lorem ipsum dolor sit aemn",
            name: "TETOCORP",
            location: "brescia"
        },
    ]
}