const Sequelize = require("sequelize");

module.exports.register = function () {
    return {
        username: {
            type: Sequelize.STRING,
            primaryKey: true,
            allowNull: false,
            references: {
                model: 'Users',
                key: 'username'
            }
        },
        session_id: {
            type: Sequelize.STRING,
            primaryKey: true,
            allowNull: false,
            references: {
                model: 'Sessions',
                key: 'session_id'
            }
        },
    };
};

module.exports.registered = function(table){

};

module.exports.default_data = function () {
    return [
        {
            username: "tetofonta",
            session_id: "asdrubale"
        },
        {
            username: "tetofonta",
            session_id: "qwerty"
        },
        {
            username: "vezzoo",
            session_id: "qwerty"
        }
    ]
}