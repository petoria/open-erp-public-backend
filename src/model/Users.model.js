const Sequelize = require("sequelize");
const crypto = require("crypto");

module.exports.register = function () {
    return {
        username: {
            type: Sequelize.STRING,
            allowNull: false,
            primaryKey: true,
            unique: "username_u"
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false
        },
        salt: {
            type: Sequelize.STRING
        },
        enable_id: {
            type: Sequelize.STRING,
            unique: "en_id_u"
        },
        enabled: {
            type: Sequelize.BOOLEAN,
        }
    };
};

module.exports.registered = function (table) {
    table.beforeCreate((user, options) => {
        let salt = Math.random().toString(36).substr(2);
        let shasum = crypto.createHash('sha1');
        shasum.update(user.password + salt);
        user.salt = salt;
        user.enabled = false;
        user.password = shasum.digest('hex');
    });
};

module.exports.default_data = function () {
    return [
        {
            username: "tetofonta",
            email: "stefano.fontana.2000@gmail.com",
            password: "60fa0613ed616aceb6c48a6352e9f71222f2fcee",
            salt: "hji7v1s8ovs",
            enable_id: "v3u5lyqr7y9rcrlrjez3hdwypkkz1v25y8q95fc1yisp7q9x63uoodwvyphkz0ld",
            enabled: 1
        },
        {
            username: "vezzoo",
            email: "luca@implast.it",
            password: "96c7944e6682fc19cf6f7a4be0fa508a3d8acbdd",
            salt: "x7lbpy53izo",
            enable_id: "3vdnfllmjkcjtherlt1f6lbxm92aeavhjgwzuhjxn8fg16gs2o4ahky4se4idt3m",
            enabled: 1
        }
    ]
};