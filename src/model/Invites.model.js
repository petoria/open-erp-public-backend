const Sequelize = require("sequelize");

module.exports.register = function () {
    return {
        invite_id: {
            type: Sequelize.STRING,
            allowNull: false,
            primaryKey: true
        },
        username: {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
                model: 'Users',
                key: 'username'
            }
        },
        session_id: {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
                model: 'Sessions',
                key: 'session_id'
            }
        }
    };
};

module.exports.registered = function (table) {

};

module.exports.default_data = function () {
    return []
}