const {connect, get} = require("sequelize-dao");
const config = require('../config');
const path = require('path');
const fastify = require('fastify')({
    logger: true
});

const reg_instance_cb = require("./callbacks/register_instance");
const new_user_cb = require("./callbacks/new_user");
const activate_cb = require("./callbacks/activate");
const login_cb = require("./callbacks/login");
const invite_cb = require("./callbacks/invite");
const accept_invite_cb = require("./callbacks/accept_invite");

config.database.order = config.database.order.map(e => path.resolve(__dirname, e));
config.port = process.env.PORT || config.port;
config.database.password = process.env.DB_PASSWORD || config.database.password;

connect(config.database).then((connection) => {

    fastify.post('/register_instance', reg_instance_cb.schema, reg_instance_cb.cb);
    fastify.post("/new_user", new_user_cb.schema,new_user_cb.cb);
    fastify.get("/activate/:activateid", activate_cb.cb);
    fastify.post("/login", login_cb.cb);
    fastify.post("/invite", invite_cb.cb);
    fastify.get("/accept_invite/:inviteid", accept_invite_cb.cb);


    fastify.listen(config.port, '0.0.0.0', function (err, address) {
        if (err) {
            fastify.log.error(err);
            process.exit(1)
        }
        fastify.log.info(`server listening on ${address}`)
    });
});