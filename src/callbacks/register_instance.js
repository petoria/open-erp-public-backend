const {connect, get} = require("sequelize-dao");
const config = require('../../config');

module.exports.cb = async function(req, res) {

    let ip = (req.headers['x-forwarded-for'] || req.raw.connection.remoteAddress).split(".").map(e => parseInt(e));
    let allowed = ip.map((e, i) => e & config.allowedPlanBackendNetworkMask[i]).reduce((a, b, i) => {
        return a && (b === config.allowedPlanBackendNetwork[i])
    }, true);
    if (!allowed) {
        res.code(403);
        return {status: "forbidden"};
    }

    try{
        await get("Sessions").build(req.body).save()
    } catch (e) {
        res.code(500);
        console.log(e);
        return {status: "Database error"}
    }

    return {status: "ok"}
};

module.exports.schema = {
    schema: {
        body: {
            type: 'object',
            required: ['session_id', 'dns', 'port', 'fingerprint'],
            properties: {
                session_id: {type: 'string'},
                dns: {type: 'string'},
                port: {type: 'integer'},
                fingerprint: {type: 'string'}
            }
        }
    }
};
