const {connect, get} = require("sequelize-dao");
const config = require('../../config');
const nodemailer = require("nodemailer");

module.exports.cb = async function (req, res) {

    let activateID = Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2)+ Math.random().toString(36).substr(2)

    try{
        let o = req.body;
        o.enable_id = activateID;
        await get("Users").build(o).save()
    } catch (e) {
        res.code(500);
        console.log(e);
        return {status: "User may exist"}
    }

    config.email.pass = process.env.MAIL_PSW;
    let transporter = nodemailer.createTransport(config.email);
    let info = await transporter.sendMail({
        from: '"open-erp =)" <fake@email.it>',
        to: `${req.body.email}`,
        subject: "Benvenuto sulla piattaforma ✔",
        text: `Attiva il tuo account all'indirizzo ${config.officialDNS}:${config.port}/activate/${activateID}`,
        html: `<b> Cosa é openrp</b><img src='https://upload.wikimedia.org/wikipedia/it/thumb/4/4e/Ciao_PX.jpg/260px-Ciao_PX.jpg'/> <a href='http://${config.officialDNS}:${config.port}/activate/${activateID}'>ciaoooo</a>`
    });

    return {status: "ok", mail: info.messageId}
};

module.exports.schema = {
    schema: {
        body: {
            type: 'object',
            required: ['username', 'password', "email"],
            properties: {
                username: {type: 'string'},
                password: {type: 'string'},
                email: {type: 'string'}
            }
        }
    }
};