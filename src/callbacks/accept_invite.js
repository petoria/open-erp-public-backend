const {connect, get} = require("sequelize-dao");
const config = require('../../config');

module.exports.cb = async function(req, res) {
    let id = req.params.inviteid;
    let users = await get("Invites").findAll({
        where: {
            invite_id: id,
            //TODO add createdat < now + 1d
        }
    });

    if(users.length < 1){
        res.code(400);
        return {status: "Cannot find invite, maybe it's expired"}
    }

    if(users.length > 1){
        res.code(500);
        return {status: "WTF!?"}
    }

    get("UserSessions").build({
        username: users[0].username,
        session_id: users[0].session_id
    }).save();

    res.code(301);
    res.header("Location", `http://${config.officialDNS}:${config.port}/login`);
    return "";
};