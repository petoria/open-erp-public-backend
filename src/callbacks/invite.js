const {connect, get} = require("sequelize-dao");
const config = require('../../config');
const {withAuth} = require("../auth");
const nodemailer = require("nodemailer");

module.exports.cb = withAuth(async (req, res, user) => {

    let alreadyExists = await get("Users").findAll({
        where: {
            username: req.body.username
        },
        include: [{
            model: get("UserSessions"),
            require: true,
            where: {
                session_id: req.body.session
            }
        }]
    });
    if(alreadyExists.length > 0){
        res.code(400);
        return {status: "Already invited"}
    }
    let user_data = await get("Users").findAll({
        where: {
            username: req.body.username
        },
    });
    if(user_data.length !== 1){
        res.code(400);
        return {status: "Not an user"}
    }

    let inviteID = Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2)+ Math.random().toString(36).substr(2)
    get("Invites").build({invite_id: inviteID, username: req.body.username, session_id: req.body.session}).save();

    config.email.pass = process.env.MAIL_PSW;

    let transporter = nodemailer.createTransport(config.email);
    let info = await transporter.sendMail({
        from: '"open-erp =)" <fake@email.it>',
        to: `${user_data[0].email}`,
        subject: "Invito a collaborare",
        text: `aaaaaaaaaaaaaaaa`,
        html: `<b> Cosa é openrp</b><img src='https://upload.wikimedia.org/wikipedia/it/thumb/4/4e/Ciao_PX.jpg/260px-Ciao_PX.jpg'/> <a href='http://${config.officialDNS}:${config.port}/accept_invite/${inviteID}'>ciaoooo</a>`
    });

    return {status: "ok"}
});

module.exports.schema = {
    schema: {
        body: {
            type: 'object',
            required: ['username', 'session'],
            properties: {
                username: {type: 'string'},
                password: {type: 'string'},
                session: {type: 'string'}
            }
        }
    }
};
