const {connect, get} = require("sequelize-dao");
const config = require('../../config');

module.exports.cb = async function(req, res) {
    let id = req.params.activateid;
    let users = await get("Users").findAll({
        attributes: ['username'],
        where: {
            enable_id: id
        }
    });

    if(users.length < 1){
        res.code(400);
        return {status: "Cannot find user id"}
    }

    if(users.length > 1){
        res.code(500);
        return {status: "WTF!?"}
    }

    users[0].enabled = true;
    await users[0].save();

    res.code(301);
    res.header("Location", `http://${config.officialDNS}:${config.port}/`);
    return "";
};