const {connect, get} = require("sequelize-dao");
const config = require('../../config');
const crypto = require('crypto');
const fetch = require('node-fetch');
const jwt = require("jsonwebtoken");
const fs = require("fs");

const privateKey = fs.readFileSync('./fingerprint/rsa-private.key');

module.exports.cb = async function (req, res) {

    let users = await get("Users").findAll({
        where: {
            username: req.body.username,
            enabled: true
        },
        include: [{
            model: get("UserSessions"),
            require: false
        }]
    });

    if (users.length < 1) {
        res.code(403);
        return {status: "User not found"}
    }
    if (users.length > 1) {
        res.code(500);
        return {status: "WTF!?"}
    }

    let computed = crypto.createHash('sha1');
    computed.update(req.body.password + users[0].salt);

    if (users[0].password === computed.digest('hex')) {
        if (req.body.session) {
            let fingerprint = await users[0].getUserSessions({ where: {session_id: req.body.session}});
            let session = await fingerprint[0].getSession();

            let publicKey = session.fingerprint;
            let buffer = Buffer.from(JSON.stringify({user: req.body.username}));
            let encrypted = crypto.publicEncrypt(publicKey, buffer).toString("base64");

            let re = await (await fetch(`${session.dns}:${session.port}/getAuth`, {method: "POST", body: encrypted})).json();
            if(!re.token){
                res.code(500);
                return {status: "Token error"}
            }
            return {
                dns: session.dns,
                port: session.port,
                token: re.token
            }
        }

        let token = jwt.sign(
            {user: users[0].username},
            privateKey,
            {algorithm: 'RS256', expiresIn: "8h"}
        );
        return {status: "ok", token: token, "sessions":
                await Promise.all(
                    (await users[0].getUserSessions()).map(async (i) => {
                        let e = await i.getSession();
                        return {id: e.session_id, desc: e.description, name: e.name, loc: e.location}
                    })
                )}
    }
    res.code(403);
    return {status: "Wrong credentials"};
};

module.exports.schema = {
    schema: {
        body: {
            type: 'object',
            required: ['username', 'password'],
            properties: {
                username: {type: 'string'},
                password: {type: 'string'},
                session: {type: 'string'}
            }
        }
    }
};
